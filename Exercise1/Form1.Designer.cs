﻿namespace Exercise1
{
    partial class MarsDietWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarsDietWindow));
            this.WeightEntryTextBox = new System.Windows.Forms.TextBox();
            this.ConvertButton = new System.Windows.Forms.Button();
            this.YourWeightLabel = new System.Windows.Forms.Label();
            this.ConvertedWeight = new System.Windows.Forms.Label();
            this.ConversionLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // WeightEntryTextBox
            // 
            this.WeightEntryTextBox.Location = new System.Drawing.Point(229, 19);
            this.WeightEntryTextBox.Name = "WeightEntryTextBox";
            this.WeightEntryTextBox.Size = new System.Drawing.Size(100, 22);
            this.WeightEntryTextBox.TabIndex = 1;
            this.WeightEntryTextBox.Text = "Enter Weight";
            this.WeightEntryTextBox.TextChanged += new System.EventHandler(this.WeightEntryLabel_TextChanged);
            // 
            // ConvertButton
            // 
            this.ConvertButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ConvertButton.Location = new System.Drawing.Point(254, 118);
            this.ConvertButton.Name = "ConvertButton";
            this.ConvertButton.Size = new System.Drawing.Size(75, 23);
            this.ConvertButton.TabIndex = 3;
            this.ConvertButton.Text = "Convert";
            this.ConvertButton.UseVisualStyleBackColor = true;
            this.ConvertButton.Click += new System.EventHandler(this.ConvertButton_Click);
            // 
            // YourWeightLabel
            // 
            this.YourWeightLabel.AutoSize = true;
            this.YourWeightLabel.Location = new System.Drawing.Point(84, 22);
            this.YourWeightLabel.Name = "YourWeightLabel";
            this.YourWeightLabel.Size = new System.Drawing.Size(140, 17);
            this.YourWeightLabel.TabIndex = 4;
            this.YourWeightLabel.Text = "Your weight on Earth";
            // 
            // ConvertedWeight
            // 
            this.ConvertedWeight.AutoSize = true;
            this.ConvertedWeight.Location = new System.Drawing.Point(84, 73);
            this.ConvertedWeight.Name = "ConvertedWeight";
            this.ConvertedWeight.Size = new System.Drawing.Size(137, 17);
            this.ConvertedWeight.TabIndex = 5;
            this.ConvertedWeight.Text = "Your weight on Mars";
            // 
            // ConversionLabel
            // 
            this.ConversionLabel.BackColor = System.Drawing.SystemColors.Window;
            this.ConversionLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.ConversionLabel.Location = new System.Drawing.Point(229, 69);
            this.ConversionLabel.Name = "ConversionLabel";
            this.ConversionLabel.Size = new System.Drawing.Size(100, 24);
            this.ConversionLabel.TabIndex = 2;
            this.ConversionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ConversionLabel.Click += new System.EventHandler(this.label1_Click);
            // 
            // MarsDietWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(384, 292);
            this.Controls.Add(this.ConvertedWeight);
            this.Controls.Add(this.YourWeightLabel);
            this.Controls.Add(this.ConvertButton);
            this.Controls.Add(this.ConversionLabel);
            this.Controls.Add(this.WeightEntryTextBox);
            this.Name = "MarsDietWindow";
            this.Text = "Mars Diet";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox WeightEntryTextBox;
        private System.Windows.Forms.Button ConvertButton;
        private System.Windows.Forms.Label YourWeightLabel;
        private System.Windows.Forms.Label ConvertedWeight;
        private System.Windows.Forms.Label ConversionLabel;
    }
}

