﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise1
{

    public partial class MarsDietWindow : Form
    {

        public int weight;
        public int sum;

        
        public MarsDietWindow()
        {
            InitializeComponent();
        }

        private void ConvertButton_Click(object sender, EventArgs e)
        {
            try
            {
                int weight;
                double weightConverted;

                weight = int.Parse(WeightEntryTextBox.Text);
                weightConverted = weight / 2.3;



                DialogResult dr = MessageBox.Show("That's all you weigh", "Lose the Weight",
                                                  MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dr == DialogResult.Yes)
                    ConversionLabel.Text = weightConverted.ToString("n3");
            }
            catch
            {
                MessageBox.Show("You cannot weight that much?  Unless... Maybe try entering a number only", "Impossible", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void WeightEntryLabel_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
